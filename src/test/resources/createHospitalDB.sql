-- Best practice MySQL as of 5.7.6
--
-- This script needs to run only once

DROP DATABASE IF EXISTS HOSPITALDB;
CREATE DATABASE HOSPITALDB;

USE HOSPITALDB;

DROP USER IF EXISTS TheUser@localhost;
CREATE USER TheUser@'localhost' IDENTIFIED WITH mysql_native_password BY 'pancake' REQUIRE NONE;
GRANT ALL ON HOSPITALDB.* TO TheUser@'localhost';

FLUSH PRIVILEGES;
