package com.kfwebstandard.jpajsfmasterdetailexample.backing;

import com.kfwebstandard.jpajsfmasterdetailexample.controller.PatientJpaController;
import com.kfwebstandard.jpajsfmasterdetailexample.entities.Patient;
import java.io.Serializable;
import java.util.List;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * JPA controller does not have getters
 *
 * @author Ken
 */
@Named("thePatients")
@RequestScoped
public class PatientBackingBean implements Serializable {

    private final static Logger LOG = LoggerFactory.getLogger(PatientBackingBean.class);

    @Inject
    private PatientJpaController patientJpaController;

    /**
     * Get list of patientspatients
     *
     * @return
     */
    public List<Patient> getPatients() {

        return patientJpaController.findPatientEntities();
    }
}
